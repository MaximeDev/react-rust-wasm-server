import React from 'react';
import logo from './logo.svg';
import './App.css';

//composant Loaded. Au clic, nous appelons la fonction exportée par notre module Rust
const Loaded = ({ wasm }) => <button onClick={wasm.greet}>Click me</button>;

//composant Unloaded. Si notre bibliothèque wasm n'a pas été chargée, nous rendons le composant Unloaded
const Unloaded = ({ loading, loadWasm }) => {
  return loading ? (
    <div>Loading...</div>
  ) : (
	//permettre à l'utilisateur d'accepter le chargement du code 
    <button onClick={loadWasm}>Load library ?</button>
  );
};

function App() {

  const [loading, setLoading] = React.useState(false);
  const [wasm, setWasm] = React.useState(null);

  /*fonction loadWasm:
  le téléchargement et la compilation de WebAssembly doivent être asynchrones.
  */
  const loadWasm = async () => {
    try {
      setLoading(true);
      const wasm = await import('rust-node-server');
      setWasm(wasm);
    } finally {
      setLoading(false);
    }
  };

  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.js</code> and save to reload.
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>

        <div>
        {wasm ? (
          <Loaded wasm={wasm} />
        ) : (
          <Unloaded loading={loading} loadWasm={loadWasm} />
        )}
        </div>

      </header>
    </div>
  );
}

export default App;
